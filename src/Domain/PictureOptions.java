package Domain;

import java.io.Serializable;

public class PictureOptions implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean printScreen;
	private boolean save;
	private boolean share;
	private boolean delete;
	private int deleteSeconds;
	
	public PictureOptions() {
		
	}
	
	public boolean isPrintScreen() {
		return printScreen;
	}
	public void setPrintScreen(boolean printScreen) {
		this.printScreen = printScreen;
	}
	public boolean isSave() {
		return save;
	}
	public void setSave(boolean save) {
		this.save = save;
	}
	public boolean isShare() {
		return share;
	}
	public void setShare(boolean share) {
		this.share = share;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public int getDeleteSeconds() {
		return deleteSeconds;
	}
	public void setDeleteSeconds(int deleteSeconds) {
		this.deleteSeconds = deleteSeconds;
	}
}
