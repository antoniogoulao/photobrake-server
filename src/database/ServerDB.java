package database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import Domain.PhotoBrakePicture;
import Domain.PhotoBrakeUser;

public class ServerDB {

	HashMap<String, PhotoBrakeUser> users = new HashMap<String, PhotoBrakeUser>();
	HashMap<String, PhotoBrakePicture> pictures = new HashMap<String, PhotoBrakePicture>();
	HashMap<String, List<PhotoBrakeUser>> friends = new HashMap<String, List<PhotoBrakeUser>>();
	HashMap<String, List<PhotoBrakePicture>> usersPictures = new HashMap<String, List<PhotoBrakePicture>>();

	public ServerDB() {

	}

	// TODO Change to boolean
	public void addUser(PhotoBrakeUser user) {
		if(!users.containsKey(user.getUsername())) {
			users.put(user.getUsername(), user);
		}
	}
	
	public PhotoBrakeUser getUser(String username) {
		return users.get(username);
	}
	
	public void addFriend(PhotoBrakeUser user, PhotoBrakeUser friend) {
		// TODO Needs approval from the friend
		// Add friend to user list
		addFriendsToDB(user, friend);
		// Add user to friend's list
		addFriendsToDB(friend, user);
	}
	
	public List<PhotoBrakeUser> getFriends(String username) {
		return friends.get(username);
	}
	
	public List<PhotoBrakePicture> getUsersPictures(String username) {
		return usersPictures.get(username);
	}
	
	public void addPicture(PhotoBrakeUser user, PhotoBrakePicture picture) {
		
		picture.setId(UUID.randomUUID().toString());
		pictures.put(picture.getId(), picture);
		
		if(usersPictures.containsKey(user.getUsername())) {
			List<PhotoBrakePicture> userPictures = usersPictures.get(user.getUsername());
			
			if(userPictures == null) {
				userPictures = new ArrayList<PhotoBrakePicture>();
				userPictures.add(picture);
				usersPictures.put(user.getUsername(), userPictures);
			} else {
				userPictures.add(picture);
			}
			
		} else {
			List<PhotoBrakePicture> userPictures = new ArrayList<PhotoBrakePicture>();
			userPictures.add(picture);
			usersPictures.put(user.getUsername(), userPictures);
		}
	}
	
	private void addFriendsToDB(PhotoBrakeUser user1, PhotoBrakeUser user2) {
		if(friends.containsKey(user1.getUsername())){
			List<PhotoBrakeUser> userFriends = friends.get(user1.getUsername());
			
			if(userFriends == null) {
				userFriends = new ArrayList<PhotoBrakeUser>();
				userFriends.add(user2);
				friends.put(user1.getUsername(), userFriends);
			} else {
				userFriends.add(user2);
			}
		} else {
			List<PhotoBrakeUser> userFriends = new ArrayList<PhotoBrakeUser>();
			userFriends.add(user2);			
			friends.put(user1.getUsername(), userFriends);
		}
	}
}
