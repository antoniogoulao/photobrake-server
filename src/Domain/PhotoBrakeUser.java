package Domain;


import java.io.Serializable;

public class PhotoBrakeUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	
	public PhotoBrakeUser(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public PhotoBrakeUser() {
		username = null;
		password = null;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	@Override
	public String toString() {
		return "User: " + username + " with Password: " + password;
	}
}
