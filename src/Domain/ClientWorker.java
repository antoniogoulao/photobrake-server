package Domain;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

import org.codehaus.jackson.map.ObjectMapper;

import database.ServerDB;

public class ClientWorker implements Runnable {

	private Socket client;
	private ServerDB database;

	//Constructor
	public ClientWorker(Socket client, ServerDB database) {
		this.client = client;
		this.database = database;
	}

	@Override
	public void run() {
		InputStream is = null;
		try {
			is = client.getInputStream();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ObjectInputStream dis = null;
		try {
			dis = new ObjectInputStream(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ActionType type = null;

		try {
			int i = dis.readInt();
			type = ActionType.values()[i];
			String username;
			switch(type) {
			case FETCHFEED:
				username = dis.readUTF();
				new ProcessFetchFeed(client, database).process(username);
				break;
			case FETCHPHOTOS:
				username = dis.readUTF();
				new ProcessFetchPhotoStream(client, database).process(username);
				break;
			case LOGIN:
				String userInfo = dis.readUTF();
				new ProcessUserLogin(client, database).process(userInfo);
				break;
			case UPLOAD:
				username = dis.readUTF();
				ObjectMapper mapper = new ObjectMapper();
				
				// TODO readValue closes socket
				PhotoBrakePicture picture = mapper.readValue(dis, PhotoBrakePicture.class);
				new ProcessUpload(client, database, picture).process(username);
				break;
			default:
				break;
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}
}
