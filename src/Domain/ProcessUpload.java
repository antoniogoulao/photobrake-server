package Domain;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import database.ServerDB;

public class ProcessUpload extends ProcessRequest {

	private PhotoBrakePicture picture;

	public ProcessUpload(Socket client, ServerDB database, PhotoBrakePicture picture) {
		super.client = client;
		super.database = database;
		this.picture = picture;
	}

	public void process(String username) {

		PhotoBrakeUser user = database.getUser(username);

		database.addPicture(user, picture);

		//		DataOutputStream out = null;
		//		try {
		//			out = new DataOutputStream(client.getOutputStream());
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		
		//		try {
		//			out.writeBoolean(true);
		//			out.flush();
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}
}
