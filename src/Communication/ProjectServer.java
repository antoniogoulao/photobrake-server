package Communication;

import java.io.IOException;
import java.net.ServerSocket;

import database.DBBootstrap;
import database.ServerDB;
import Domain.ClientWorker;
import Domain.PhotoBrakeUser;


public class ProjectServer {

	private static ServerDB database;

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		database = DBBootstrap.getInstance().populate();

		ServerSocket server = null;
		try {
			server = new ServerSocket(4321);
		} catch (IOException e) {
			System.out.println("Could not listen on port 4321");
		}
		// DEBUG Check IP Addpress
		//		Enumeration<NetworkInterface> e = null;
		//		try {
		//			e = NetworkInterface.getNetworkInterfaces();
		//		} catch (SocketException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}
		//		while(e.hasMoreElements())
		//		{
		//			NetworkInterface n = (NetworkInterface) e.nextElement();
		//			Enumeration<InetAddress> ee = n.getInetAddresses();
		//			while (ee.hasMoreElements())
		//			{
		//				InetAddress i = (InetAddress) ee.nextElement();
		//				System.out.println(i.getHostAddress());
		//			}
		//		}

		while(true) {
			ClientWorker w;

			try {
				w = new ClientWorker(server.accept(), database);
				System.out.println("New connection");
				Thread t = new Thread(w);
				t.start();
			} catch (IOException ex) {
				System.out.println("Accept failed: 4321");
				System.exit(-1);
			}
		}
	}
}
