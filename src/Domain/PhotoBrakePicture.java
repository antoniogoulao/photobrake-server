package Domain;

import java.io.Serializable;
import java.util.Date;

public class PhotoBrakePicture implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String description;
	private Date date;
	private byte[] data;
	private PictureOptions options;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public PictureOptions getOptions() {
		return options;
	}
	public void setOptions(PictureOptions options) {
		this.options = options;
	}
}
