package Domain;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import database.ServerDB;

public class ProcessUserLogin extends ProcessRequest {
	
	
	
	public ProcessUserLogin(Socket client, ServerDB database) {
		super.client = client;
	    super.database = database;
	}
	
	public void process(String userInfo) {
		String[] data = new String(userInfo).split("0x1b");

		PhotoBrakeUser user = database.getUser(data[0]);

		LogInState answer;
		if(user == null) {
			answer = LogInState.NOEXIST;
		} else if (!user.getPassword().equals(data[1])) {
			answer = LogInState.NOTOK;
		} else {
			answer = LogInState.OK;
		}

		DataOutputStream out = null;
		try {
			out = new DataOutputStream(client.getOutputStream());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			out.writeInt(answer.ordinal());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
