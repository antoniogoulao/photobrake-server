package database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import Domain.PhotoBrakePicture;
import Domain.PhotoBrakeUser;

public class DBBootstrap {
	
	private static DBBootstrap instance;

	protected DBBootstrap() {
		
	}
	
	public static DBBootstrap getInstance() {
		if(instance == null) {
			instance = new DBBootstrap();
		}
		return instance;
	}
	
	public ServerDB populate() {
		ServerDB database = new ServerDB();
		PhotoBrakeUser qw = new PhotoBrakeUser("qw", "qw");
		PhotoBrakeUser as = new PhotoBrakeUser("as", "as");
		PhotoBrakeUser zx = new PhotoBrakeUser("zx", "zx");
		PhotoBrakeUser po = new PhotoBrakeUser("po", "po");
		PhotoBrakeUser lk = new PhotoBrakeUser("lk", "lk");
		PhotoBrakeUser mn = new PhotoBrakeUser("mn", "mn");
		PhotoBrakeUser ai = new PhotoBrakeUser("ai", "ai");
		PhotoBrakeUser ui = new PhotoBrakeUser("ui", "ui");
		PhotoBrakeUser oi = new PhotoBrakeUser("oi", "oi");
		PhotoBrakePicture img1 = new PhotoBrakePicture(); 
		PhotoBrakePicture img2 = new PhotoBrakePicture();
		PhotoBrakePicture img3 = new PhotoBrakePicture();
		PhotoBrakePicture img4 = new PhotoBrakePicture();
		PhotoBrakePicture img5 = new PhotoBrakePicture();
		PhotoBrakePicture img6 = new PhotoBrakePicture();
		PhotoBrakePicture img7 = new PhotoBrakePicture();
		try {
			img1.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img1.jpg").toPath()));
			img2.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img2.jpg").toPath()));
			img3.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img3.jpg").toPath()));
			img4.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img4.jpg").toPath()));
			img5.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img5.jpg").toPath()));
			img6.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img6.jpg").toPath()));
			img7.setData(Files.readAllBytes(new File("/home/antonio/workspace/PhotoBrake Server/img/img7.jpg").toPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		database.addUser(qw);
		database.addUser(as);
		database.addUser(zx);
		database.addUser(po);
		database.addUser(lk);
		database.addUser(mn);
		database.addUser(ai);
		database.addUser(ui);
		database.addUser(oi);
		database.addFriend(qw, as);
		database.addFriend(qw, zx);
		database.addFriend(as, zx);
		database.addFriend(qw, po);
		database.addFriend(qw, lk);
		database.addFriend(qw, mn);
		database.addFriend(qw, ai);
		database.addFriend(qw, ui);
		database.addFriend(as, oi);
		database.addPicture(zx, img1);
		database.addPicture(zx, img2);
		database.addPicture(as, img3);
		database.addPicture(qw, img4);
		database.addPicture(qw, img5);
		database.addPicture(qw, img6);
		database.addPicture(qw, img7);
		
		return database;
	}
}
