package Domain;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import org.codehaus.jackson.map.ObjectMapper;

import database.ServerDB;

public class ProcessFetchPhotoStream extends ProcessRequest {

	public ProcessFetchPhotoStream(Socket client, ServerDB database) {
		super.client = client;
		super.database = database;
	}

	public void process(String username) {
		ArrayList<PhotoBrakePicture> pictures = (ArrayList<PhotoBrakePicture>) database.getUsersPictures(username);
		
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(client.getOutputStream());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(out, pictures);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
